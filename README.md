# Aria2

本项目为基于 alpine 的 aria2 镜像，仅安装和定制 aria2 ，不包含 aria2 gui 工具。



默认使用 [aria2.conf](aria2.conf) 作为默认的配置文件。若需要替换该配置文件，则将目标配置文件映射至 `/etc/aria2/aria2.conf` 即可。

aria2 的配置内容请根据 [aria2 官方网站](https://aria2.github.io/manual/en/html/aria2c.html)进行配置。


## 使用


```yaml
services:

  aria2:
    image: yanglw/aria2:latest
    restart: unless-stopped
    ports: 
      - "6800:6800" # RPC 端口
      - "6881:6881" # BT 端口
      - "6882:6882/udp" # DHT 端口
    volumes:
      # aria2 的会话记录文件。若使用默认配置文件，则必须创建该文件！！！
      - path/to/session/file:/data/aria2/session/aria2.session
      # aria2 的下载目录。
      - path/to/downloads/:/data/aria2/downloads/
      # 自定义 aria2 的配置文件，可选。
      # - ./aria2/config/arai2.conf:/etc/aria2/aria2.conf
      # aria2 dht 文件存储目录，可选。
      # - ./aria2/dht/:/data/aria2/dht/
```

**注意**

若使用默认的配置文件，则需要创建一个文件名为 `aria2.session` 的空白文件，否则镜像无法启动。
