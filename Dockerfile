FROM alpine:latest

LABEL AUTHOR=yanglw<weigeku8@gmail.com>

RUN apk add --update aria2 --no-cache \
 && rm -rf /var/cache/apk/ /var/lib/apk/ /etc/apk/cache/ \
 && mkdir -p /etc/aria2/
COPY aria2.conf /etc/aria2/aria2.conf

ENTRYPOINT ["aria2c"]
CMD ["--conf-path=/etc/aria2/aria2.conf"]
